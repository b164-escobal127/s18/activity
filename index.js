let trainer = {
	name: 'Kjames Ketchup',
	age: 18,
	Friends: {
		name: ['Misty', 'Brock'],
		location: ['Heonn','Neonn'],

	},
	pokemon: ['Pikachu', 'Arceus', 'Mewtwo', 'Giratina', 'Rayquaza', 'Dialga'],
	talk: function(){
		console.log(`Pikachu, I choose you!`)
	}


}
console.log(trainer);

console.log('Result of dot notation:')
console.log(trainer.name)
console.log('Result of square bracket notation')
console.log(trainer['pokemon'])
console.log('Result of talk method')
trainer.talk();


function Pokemon(name, level) {
	

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 1.5 * level;


	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to`, target.health - this.attack)

	}

	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}



}


let pikachu = new Pokemon("Pikachu", 6);
console.log(pikachu)
let mewtwo = new Pokemon("Mewtwo", 99);
console.log(mewtwo)
let garados = new Pokemon("Garados", 10);
console.log(garados)

pikachu.tackle(mewtwo);

mewtwo.tackle(pikachu)











